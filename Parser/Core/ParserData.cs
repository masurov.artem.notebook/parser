﻿using System.Linq;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Parser.Core.Interface;
using Parser.Data;

namespace Parser.Core
{
    public class ParserData : IParserData<CompanyCard>
    {
        public CompanyCard Parse(IHtmlDocument document, CompanyCard cCard)
        {
            // Get General table
            var generalData = document.QuerySelectorAll("table tr");

            // Get table summary inspection
            var summaryInspection = document.QuerySelectorAll(".inspection_table tr");

            // Get CheckNumber
            cCard.CheckNumber = new string(document.QuerySelector(".page_title").Text().Where(char.IsDigit).ToArray());

            // Get Status
            cCard.VerificationStatus = document.QuerySelector("td.text-success").Text();
            
            // Get Scope of control, Supervisor, TypeOfInspection, Sanction
            foreach (var element in summaryInspection)
            {
                var verifiable = element.QuerySelector("td")?.Text();
                var assignable = element.QuerySelector("td:last-child")?.Text();

                if (verifiable == null || assignable == null)
                {
                    break;
                }
                if (verifiable.Contains("Контролюючий орган"))
                {
                    cCard.Supervisor = assignable;
                }
                if (verifiable.Contains("Сфера контролю"))
                {
                    cCard.ScopeOfControl = assignable;
                }
                if (verifiable.Contains("Тип перевірки"))
                {
                    cCard.TypeOfInspection = assignable;
                }
                if (verifiable.Contains("Заходи впливу та санкції"))
                {
                    cCard.Sanctions = assignable;
                }
                if (verifiable.Contains("Дати"))
                {
                    cCard.Dates = assignable;
                }
            }

            // Get Identification code, Get DegreeOfRisk
            foreach (var element in generalData)
            {
                var verifiable = element.QuerySelector("td")?.Text();
                var assignable = element.QuerySelector("td:last-child")?.Text();

                if (verifiable == null || assignable == null)
                {
                    break;
                }
                if (verifiable.Contains("Ідентифікаційний код"))
                {
                    cCard.IdentificationCode = assignable;
                }
                if (verifiable.Contains("Ступінь ризику"))
                {
                    cCard.DegreeOfRisk = assignable; 
                }
            }
            return cCard;
        }
    }
}