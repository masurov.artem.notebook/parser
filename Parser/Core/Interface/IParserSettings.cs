﻿namespace Parser.Core.Interface
{
    public interface IParserSettings
    {
        public string Url { get; set; }
        public string Prefix { get; set; }
        public string BaseUrl { get; set; }

        // Start page for parsing
        public int StartPage { get; set; }
        
        // End page for parsing
        public int EndPage { get; set; }
    }
}