﻿using AngleSharp.Html.Dom;
using Parser.Data;

namespace Parser.Core.Interface
{
    public interface IParserData<out T> where T: class
    {
        T Parse(IHtmlDocument document, CompanyCard cCard);
    }
}