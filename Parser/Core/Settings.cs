﻿using Parser.Core.Interface;

namespace Parser.Core
{
    public class Settings: IParserSettings
    {
        public Settings(int startPage)
        {
            StartPage = startPage;
        }
        public Settings(int startPage, int endPage)
        {
            StartPage = startPage;
            EndPage = endPage;
        }
        public string Url { get; set; } =
            "https://inspections.gov.ua/inspection/all-unplanned?planning_period_id=2&page=";
        public string BaseUrl { get; set; } = "https://inspections.gov.ua";
        public string Prefix { get; set; } = "{CurrentId}";
        public int StartPage { get; set; }
        public int EndPage { get; set; }
    }
}