﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using HtmlAgilityPack;
using Parser.Core.Interface;
using Parser.Data;
using Parser.Helper;

namespace Parser.Core
{
    public class ParserPages<T> where T : class
    {
        private readonly IParserData<T> _parserData;
        private readonly IParserSettings _parserSettings;
        private readonly HtmlDomLoader _loader;

        public Action<object> OnCompleted;

        public ParserPages(IParserData<T> parserData)
        {
            _parserData = parserData;
        }

        public ParserPages(IParserData<T> parserData, IParserSettings parserSettings) : this(parserData)
        {
            _parserSettings = parserSettings;
            _loader = new HtmlDomLoader(_parserSettings);
        }

        public void Start()
        {
            Console.WriteLine("Parsing start");
            Worker();
        }
        
        private async void Worker()
        {
            var export = new CsvExport();
            var domParser = new HtmlParser();
            var currentPage = _parserSettings.StartPage;
            // Parsing of all pages or by a specified parameter
            while (currentPage <= _parserSettings.EndPage && currentPage > 0 || currentPage > 0)
            {
                // Get data by from page by Id
                var source = await _loader.GetSourceByPageId(currentPage);

                // Checking the last page
                var document = await domParser.ParseDocumentAsync(source);
                var activePage = document.QuerySelectorAll(".pagination").Children(".active").FirstOrDefault()?.TextContent;
                // If the parser refers to a page that does not exist then "Return"
                if (currentPage != int.Parse(activePage ?? string.Empty))
                {
                    export.ExportToFile("companyCards.csv");
                    return;
                }

                // Get all links from the page
                var links = HtmlAgilityPack(source);

                Console.Write($"Page {currentPage}");
                
                foreach (var currentLink in links)
                {
                    // Create object card
                    var companyCard = new CompanyCard { LinkResultsCard = _parserSettings.BaseUrl + currentLink };
                    
                    // Get data by from page by link
                    source = await _loader.GetSourceByLink(currentLink);

                    // Parsing data
                    document = await domParser.ParseDocumentAsync(source);
                    var result = (CompanyCard)(object)_parserData.Parse(document, companyCard);

                    //Save to a temporary source
                    SaveCardToCsv(export, result);
                    Console.Write("..");
                    
                }
                Console.Write($"done\n");
                currentPage++;
            }
            // SaveToCsvFile
            export.ExportToFile("companyCards.csv");
            OnCompleted?.Invoke(this);
            }

        // Extension to get all links
        public IEnumerable<string> HtmlAgilityPack(string html)
        {
            var htmlSnippet = new HtmlDocument();
            htmlSnippet.LoadHtml(html);

            var hrefTags = new List<string>();

            foreach (HtmlNode link in htmlSnippet.DocumentNode.SelectNodes("//a[@href]"))
            {
                HtmlAttribute att = link.Attributes["href"];
                if (att.Value.Contains("/inspection/view?id="))
                {
                    hrefTags.Add(att.Value);
                }
            }

            return hrefTags;
        }

        public void SaveCardToCsv(CsvExport export, CompanyCard card)
        {
            export.AddRow();
            export["Iдентифікаційний код"] = card.IdentificationCode;
            export["Контролюючий орган"] = card.Supervisor;
            export["Сфера контролю"] = card.ScopeOfControl;
            export["Перевірка №"] = card.CheckNumber;
            export["Статус перевірки"] = card.VerificationStatus;
            export["Ступінь ризику"] = card.DegreeOfRisk;
            export["Тип перевірки"] = card.TypeOfInspection;
            export["Санкції (грн.)"] = card.Sanctions;
            export["Дати проведення"] = card.Dates;
            export["Посилання на картку з результатами"] = card.LinkResultsCard;
        }
    }
}