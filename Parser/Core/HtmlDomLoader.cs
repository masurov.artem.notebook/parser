﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Parser.Core.Interface;

namespace Parser.Core
{
    public class HtmlDomLoader
    {
        private readonly HttpClient _client;
        private readonly string _url;
        private readonly IParserSettings _parserSettings;

        public HtmlDomLoader(IParserSettings parserSettings)
        {
            _client = new HttpClient();
            _parserSettings = parserSettings;
            _url = $"{_parserSettings.Url}{_parserSettings.Prefix}";
        }
        
        public async Task<string> GetSourceByPageId(int id)
        {
            var currentUrl = _url.Replace("{CurrentId}", id.ToString());
            try
            {
                var response = _client.GetAsync(currentUrl).Result;
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    return await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return null;
        }

        public async Task<string> GetSourceByLink(string link)
        {
            var currentUrl = link.Insert(0, _parserSettings.BaseUrl);
            try
            {
                var response = _client.GetAsync(currentUrl).Result;

                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    return await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            return null;
        }

    }
}