﻿using System;
using Parser.Core;
using Parser.Data;

namespace Parser
{
    internal class Program
    {
        private static ParserPages<CompanyCard> _parser;
        
        private static void Main(string[] args)
        {
            //_parser = new ParserPages<CompanyCard>(
            //    new ParserData(),
            //    new Settings(1));

            _parser = new ParserPages<CompanyCard>(
               new ParserData(),
               new Settings(3662, 3670));

            _parser.OnCompleted += OnCompleted;

            _parser.Start();
        }
        private static void OnCompleted(object obj)
        {
            Console.WriteLine("done!");
        }
    }
   
}
