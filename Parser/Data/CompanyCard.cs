﻿namespace Parser.Data
{
    public class CompanyCard
    {
        public string IdentificationCode { get; set; }
        public string Supervisor { get; set; }
        public string ScopeOfControl { get; set; }
        public string CheckNumber { get; set; }
        public string VerificationStatus { get; set; }
        public string DegreeOfRisk { get; set; }
        public string TypeOfInspection { get; set; }
        public string Sanctions { get; set; }
        public string Dates { get; set; }
        public string LinkResultsCard { get; set; }

        public CompanyCard()
        {

        }
    }
}